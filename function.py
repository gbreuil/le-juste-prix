import random
import CONSTANTE
import math
import json
from datetime import datetime


def create_random_price():
    b = random.uniform(CONSTANTE.a[0], CONSTANTE.a[1])
    b = math.trunc((10.0 ** 2) * b) / 10.0 ** 2
    return b


def display_game():
    print('-' * 50)
    print("Vous avez :", CONSTANTE.temps, " seconde pour trouver le prix")
    print('-' * 50)


def display_price(price):
    print("Le prix est de: ", price)


def endgame_timer(z):
    if z <= 0:
        print("Le temps est coulé ## VOUS AVEZ PERDU!##")
        return False


def display_time_left(z):
    print("il vous reste : ", math.trunc(z), " secondes. ")


def verify_price(price, price_given):
    if price_given > price:
        return 1
    elif price_given < price:
        return 2
    elif price_given == price:
        return 3


def end_game(int):
    if int != 3:
        return False
    else:
        return True


def restart():
    n=False
    while n==False:
        print("Vous voulez faire un autre tour??? \n taper o pour rejouer ou n pour quitter")
        d = input("o/n ? :")
        dd = d.lower()
        if dd == 'o':
            return True
        elif dd == 'n':
            return False
        else:
            n = False


def display_answer(answer):
    if answer == 1:
        print("Le prix est inférieur! \n")
    if answer == 2:
        print("Le prix est supérieur! \n")
    if answer == 3:
        print("Bravo! Tu as trouvé le prix\n")


def ask_value():
    p=False
    while p==False:
        try:
            print("Saisir le prix à trouver :")
            x = float(input())
            assert x >= 0
            return x
            p = True
        except ValueError:
            print("Veuillez entrer un chiffre ")
        except AssertionError:
            print("il faut que ton chiffre soit superieur ou egal à zero")


def write_file(answer, temps, compteur):
    with open("data.txt", "a+") as fichier:
        if answer == 1:
            fichier.write("Perdu ; 0s; "+ str(compteur) + "essais\n"+ "###########################\n")
        if answer == 0:
            fichier.write("Gagné ; "+ str(math.trunc(temps)) +"s; " + str(compteur) + "essais\n" + "###########################\n")
        fichier.close()


def write_json(bool,temps,essai):
    dic_score={"Gagnee":bool,"Duree":round(temps,2),"Nbre_essai":essai}
    with open("score.json","a") as fichier:
        json.dump(dic_score,fichier)


def get_time():
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M")
    print("Date et Heure :", dt_string)

